package com.heli.tinnews.Vehicles;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.heli.tinnews.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SavedCarriersFragment extends Fragment {


    public static SavedCarriersFragment newInstance() {
        
        Bundle args = new Bundle();
        SavedCarriersFragment fragment = new SavedCarriersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_saved_carriers, container, false);

        // Use listview with CustomizedListAdapter to display saved vehicle items

        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.sedan);
        images.add(R.drawable.cargo_van);
        images.add(R.drawable.sedan);
        List<String> descriptions = new ArrayList<>();
        descriptions.add("Lida");
        descriptions.add("Tony");
        descriptions.add("HeLi");
        List<String> prices = new ArrayList<>();
        prices.add("$20.99");
        prices.add("$40.99");
        prices.add("$40.99");
        CustomListAdapter adapter = new CustomListAdapter(view.getContext(), images, descriptions, prices);
        ListView listView = ( ListView ) view.findViewById(R.id.offerList);
        listView.setAdapter(adapter);
        return  view;
    }

}
