package com.heli.tinnews.common;

import android.os.Bundle;

public interface UIFragmentManager {
    void doFragmentTransaction(UIBasicFragment basicFragment);

    void startActivityWithBundle(Class<?> clazz, boolean isFinished, Bundle bundle);

    void showSnackBar(String message);
}
