package com.heli.tinnews.Vehicles;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.heli.tinnews.R;

import java.util.List;

public class PaymentListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> descriptions;
    private final List<Integer> images;
    /**
     *
     * @param context
     * @param images        List of Images source as Integer
     * @param descriptions  List of String for descriptions
     */
    public PaymentListAdapter(Context context, List<Integer> images, List<String> descriptions) {
        super(context, R.layout.offer_item, descriptions);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.descriptions = descriptions;
        this.images = images;

    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rowView = inflater.inflate(R.layout.payment_item, null,true);
        ImageView imageView = rowView.findViewById(R.id.payment_image);
        TextView txtTitle = rowView.findViewById(R.id.payment_text);

        txtTitle.setText(descriptions.get(position));
        imageView.setImageResource(images.get(position));
        // Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(imageView);
        return rowView;
    }
}