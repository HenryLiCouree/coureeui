package com.heli.tinnews.Vehicles;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.heli.tinnews.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final List<String> descriptions;
    private final List<Integer> images;
    private final List<String> prices;

    /**
     *
     * @param context
     * @param images        List of Images source as Integer
     * @param descriptions  List of String for descriptions
     * @param prices        List of String of price
     */
    public CustomListAdapter(Context context, List<Integer> images, List<String> descriptions, List<String> prices) {
        super(context, R.layout.offer_item, descriptions);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.descriptions = descriptions;
        this.images = images;
        this.prices = prices;
    }

    public View getView(int position,View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View rowView = inflater.inflate(R.layout.offer_item, null,true);
        ImageView imageView = rowView.findViewById(R.id.icon);
        TextView txtTitle =  rowView.findViewById(R.id.offerDetail);
        TextView extratxt =  rowView.findViewById(R.id.priceText);

        txtTitle.setText(descriptions.get(position));
        imageView.setImageResource(images.get(position));
        // Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(imageView);
        extratxt.setText(prices.get(position));
        return rowView;

    }
}