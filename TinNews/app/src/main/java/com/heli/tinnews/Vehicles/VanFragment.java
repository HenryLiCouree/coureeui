package com.heli.tinnews.Vehicles;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heli.tinnews.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VanFragment extends Fragment {


    public static VanFragment newInstance() {
        
        Bundle args = new Bundle();
        
        VanFragment fragment = new VanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_van, container, false);
    }

}
