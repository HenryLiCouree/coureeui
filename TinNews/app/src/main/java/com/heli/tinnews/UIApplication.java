package com.heli.tinnews;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class UIApplication extends Application {
    @Override
    public void onCreate() {
        //Register to Stetho for debug
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

}
