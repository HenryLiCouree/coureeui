package com.heli.tinnews.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.heli.tinnews.R;
import com.heli.tinnews.Vehicles.BikeFragment;
import com.heli.tinnews.Vehicles.PickUpFragment;
import com.heli.tinnews.Vehicles.SavedCarriersFragment;
import com.heli.tinnews.Vehicles.SedanFragment;
import com.heli.tinnews.Vehicles.SuvFragment;
import com.heli.tinnews.Vehicles.TruckFragment;
import com.heli.tinnews.Vehicles.VanFragment;

public class ContainerFragment extends UIBasicFragment {
    private int pageIndex;
    public static final int BIKE_PAGE = 0;
    public static final String BIKE_PAGE_TAG = "bike_page";
    public static final int SEDAN_PAGE = 1;
    public static final String SEDAN_PAGE_TAG = "sedan_page";
    public static final int SUV_PAGE = 2;
    public static final String SUV_PAGE_TAG = "suv_page";
    public static final int PICKUP_PAGE = 3;
    public static final String PICKUP_PAGE_TAG = "pickup_page";
    public static final int VAN_PAGE = 4;
    public static final String VAN_PAGE_TAG = "van_page";
    public static final int TRUCK_PAGE = 5;
    public static final String TRUCK_PAGE_TAG = "truck_page";
    public static final int SAVED_CARRIERS_PAGE = 6;
    public static final String SAVED_CARRIERS_PAGE_TAG = "saved_carriers_page";

    private Fragment initFragment;

    public static ContainerFragment newInstance(int pageIndex) {
        // Create containerFragment bind with specific vehicle fragments
        ContainerFragment containerFragment = new ContainerFragment();
        containerFragment.pageIndex = pageIndex;
        containerFragment.initFragment = createInitFragmentByIndex(pageIndex);
        return containerFragment;
    }

    private static Fragment createInitFragmentByIndex(int pageIndex) {
        switch (pageIndex) {
            case BIKE_PAGE:
                return BikeFragment.newInstance();
            case SEDAN_PAGE:
                return SedanFragment.newInstance();
            case SUV_PAGE:
                return SuvFragment.newInstance();
            case PICKUP_PAGE:
                return PickUpFragment.newInstance();
            case VAN_PAGE:
                return VanFragment.newInstance();
            case TRUCK_PAGE:
                return TruckFragment.newInstance();
            case SAVED_CARRIERS_PAGE:
                return SavedCarriersFragment.newInstance();
            default:
                throw new IndexOutOfBoundsException();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // will transfer from container fragment to specific fragment during onActivityCreated
        if (initFragment != null && !initFragment.isAdded()) {
            getChildFragmentManager().beginTransaction().replace(R.id.child_fragment_container, initFragment, getCurrentTag(pageIndex))
                    .commit();
        }
    }

    public static String getCurrentTag(int position) {
        switch (position) {
            case BIKE_PAGE:
                return BIKE_PAGE_TAG;
            case SEDAN_PAGE:
                return SEDAN_PAGE_TAG;
            case SUV_PAGE:
                return SUV_PAGE_TAG;
            case PICKUP_PAGE:
                return PICKUP_PAGE_TAG;
            case VAN_PAGE:
                return VAN_PAGE_TAG;
            case TRUCK_PAGE:
                return TRUCK_PAGE_TAG;
            case SAVED_CARRIERS_PAGE:
                return SAVED_CARRIERS_PAGE_TAG;
            default:
                return null;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.child_fragment_container, container, false);
    }
}

