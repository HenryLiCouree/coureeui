package com.heli.tinnews.Vehicles;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.heli.tinnews.MainActivity;
import com.heli.tinnews.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SedanFragment extends Fragment {


    ListView lView;

    ListAdapter lAdapter;
    public static SedanFragment newInstance() {
        
        Bundle args = new Bundle();
        
        SedanFragment fragment = new SedanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_sedan, container, false);
//        lView = (ListView) view.findViewById(R.id.sedanList);
//
//        lAdapter = new ListAdapter(this.getContext(), new String[]{}, new String[]{}, new int[]{});
//
//        lView.setAdapter(lAdapter);
//
//        lView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//            }
//        });
        return view;
    }

}
