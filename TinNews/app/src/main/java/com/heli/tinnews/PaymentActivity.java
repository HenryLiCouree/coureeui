package com.heli.tinnews;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.heli.tinnews.Vehicles.CustomListAdapter;
import com.heli.tinnews.Vehicles.PaymentListAdapter;

import java.util.ArrayList;
import java.util.List;

public class PaymentActivity extends AppCompatActivity {
    ListView listView;
    PaymentListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        // Use listview with paymentListAdapter to display payment items

        listView = findViewById(R.id.paymentList);
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.visa);
        images.add(R.drawable.mastercard);
        images.add(R.drawable.couree_pink);

        List<String> descriptions = new ArrayList<>();
        descriptions.add("VISA 1234");
        descriptions.add("Mastercard 2345");
        descriptions.add("Couree Balance");
        // Custimized paymentlist adapter
        adapter = new PaymentListAdapter(this, images, descriptions);
        listView.setAdapter(adapter);
        ImageView forward = findViewById(R.id.forwardButton);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
