package com.heli.tinnews;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.heli.tinnews.common.ContainerFragment;
import com.heli.tinnews.common.UIBasicActivity;
import com.heli.tinnews.common.UIBasicFragment;
import com.heli.tinnews.common.UIFragmentPagerAdapter;

public class MainActivity extends UIBasicActivity {
    private ViewPager viewPager;
    private UIFragmentPagerAdapter adapter;
    private TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Main Activity layout contains mapImage, tabs, viewPager, payment bar and confirm button
        final ImageView mapImage = findViewById(R.id.mapImage);
        // Configure viewPager and bind it with containerFragments
        // (for each container Fragment, will be initialized to specific fragment)
        viewPager = findViewById(R.id.viewpager);
        adapter = new UIFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(UIFragmentPagerAdapter.FRAGMENT_NUMBER);
        final View paymentBar = findViewById(R.id.payment);
        final Button button = findViewById(R.id.confirmButton);
        // get tab view and bind with onTabSelectedListener
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                // Saved carriers page won't contains payment bar and confirm button
                if (id != ContainerFragment.SAVED_CARRIERS_PAGE) {
                    paymentBar.setVisibility(View.VISIBLE);
                    button.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(id);
                } else {
                    paymentBar.setVisibility(View.GONE);
                    button.setVisibility(View.GONE);
                    viewPager.setCurrentItem(id);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        // when click payment select, will go to payment Activity
        View paymentSelect = findViewById(R.id.paymentSelect);
        paymentSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PaymentActivity.class);
                startActivity(intent);
            }
        });
        // when click payment select, will go to confirm Activity
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConfirmActivity.class);
                startActivity(intent);
            }
        });

        // when click extend Line, will hide mapImage, and display in full screen
        View extendLine = findViewById(R.id.extendLine);
        final View tabs = findViewById(R.id.tabs);
        extendLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mapImage.getVisibility() == View.VISIBLE) {
                    mapImage.setVisibility(View.GONE);
                    tabs.setVisibility(View.GONE);
                } else {
                    mapImage.setVisibility(View.VISIBLE);
                    tabs.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void showSnackBar(String message) {

    }
    private FragmentManager getCurrentChildFragmentManager() {
        return adapter.getItem(viewPager.getCurrentItem()).getChildFragmentManager();
    }
    @Override
    public void doFragmentTransaction(UIBasicFragment basicFragment) {
        FragmentTransaction fragmentTransaction = getCurrentChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(
                R.id.child_fragment_container,
                basicFragment,
                basicFragment.getFragmentTag()).addToBackStack(null).commit();
    }

}
